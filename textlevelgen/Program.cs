﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace textlevelgen
{
    class Program
    {

        public static void generateLevels(int levelnumber)
        {
            char[,] levelmap;

            LevelGen levelGen = new LevelGen();
            levelmap = levelGen.getFinalMap();
            
            int choice = LevelGen.getRandomInt(0, 4);

            if (choice == 2)
            {
                levelmap = rotateCharArray(levelmap);
            }
            else if (choice == 3)
            {
                levelmap = rotateCharArray(levelmap);
                levelmap = rotateCharArray(levelmap);
            }
            else if (choice == 4)
            {
                levelmap = rotateCharArray(levelmap);
                levelmap = rotateCharArray(levelmap);
                levelmap = rotateCharArray(levelmap);
            }

            string[] lines = new string[40];

            for (int row = 0; row < levelmap.GetLength(0); row++)
            {
                string column = "";
                for (int col = 0; col < levelmap.GetLength(1); col++)
                {
                    column += levelmap[row, col];
                }
                lines[row] = column;
            }

            string path = Directory.GetParent(System.Reflection.Assembly.GetExecutingAssembly().Location).FullName; // return the application.exe current folder

            string fileName = Path.Combine(path, "../../OutputTexts/");


            int seed1 = LevelGen.getRandomInt(0, 99);

            int seed2 = LevelGen.getRandomInt(0, 99);

            fileName += "level" + "_" + seed1 + seed2 + choice + "_" + levelnumber + ".txt";
            System.IO.File.WriteAllLines(fileName, lines);
        }

        public static char[,] rotateCharArray (char[,] array)
        {
            char[,] newArray = new char[40, 40];

            for (int i = 39; i >= 0; --i)
            {
                for (int j = 0; j < 40; ++j)
                {
                    newArray[j, 39 - i] = array[i, j];
                }
            }

            return newArray;
        }
        


        static void Main(string[] args)
        {
            for (int i = 0; i < 50; i++)
            {
                generateLevels(i);
            }
        }
    }

    class LevelGen
    {
        // 0 = outer wall
        // g = grass A
        // G = grass B
        // d = dirt A
        // D = dirt B
        // t = concrete tile
        // r = rock A
        // R = rock B
        // s = snow A
        // S = snow B
        // i = ice A
        // I = ice B

        char outer_wall_A = '0';
        char grass_A = 'g';
        char grass_B = 'G';
        char dirt_A = 'd';
        char dirt_B = 'D';
        char concrete_tile_A = 't';
        char rock_A = 'r';
        char rock_B = 'R';
        char snow_A = 's';
        char snow_B = 'S';
        char ice_A = 'i';
        char ice_B = 'I';

        public int[,] dirt_layer;
        public int[,] grass_layer;
        public int[,] snow_layer;
        public int[,] ice_layer;
        public int[,] roads_layer;
        public int[,] blocks_layer;
        public int[,] outer_bounds;

        public char[,] final_map;

        public int grid_size;

        private static readonly Random getRandom = new Random();

        public LevelGen()
        {
            grid_size = 40;

            dirt_layer = new int[grid_size, grid_size];
            grass_layer = new int[grid_size, grid_size];
            snow_layer = new int[grid_size, grid_size];
            ice_layer = new int[grid_size, grid_size];
            roads_layer = new int[grid_size, grid_size];
            blocks_layer = new int[grid_size, grid_size];
            outer_bounds = new int[grid_size, grid_size];

            final_map = new char[grid_size, grid_size];

            //generate_dirt();
            //generate_grass();
            //generate_snow();
            //generate_ice();
            generate_roads();
            dirt_layer = random_map();
            grass_layer = random_map();
            snow_layer = random_map();
            ice_layer = random_map();
            //blocks_layer = random_map();

            //generate_blocks();
            generate_outer_bounds();

            generate_final_map();
        }

        public int[,] random_map()
        {
            int[,] maprandom = new int[grid_size, grid_size];

            for (int row = 0; row < grid_size; row++)
            {
                for (int col = 0; col < grid_size; col++)
                {
                    int testNum = getRandomInt(2, 5);
                    if (testNum <= 3)
                    {
                        maprandom[row, col] = 1;
                    }
                    else if (testNum >= 4)
                    {
                        maprandom[row, col] = 0;
                    }
                    

                }
            }

            return maprandom;
        }

        private void generate_final_map()
        {
            // set to empty

            for (int row = 0; row < grid_size; row++)
            {
                for (int col = 0; col < grid_size; col++)
                {
                    final_map[row, col] = 'E';
                }
            }

            for (int row = 0; row < grid_size; row++)
            {
                for (int col = 0; col < grid_size; col++)
                {
                    if (dirt_layer[row,col] == 1)
                    {
                        int choice = getRandomInt(0, 1);
                        if (choice == 0)
                        {
                            final_map[row, col] = dirt_A;
                        }
                        else
                        {
                            final_map[row, col] = dirt_B;
                        }

                    }
                    if (grass_layer[row, col] == 1)
                    {
                        int choice = getRandomInt(0, 1);
                        if (choice == 0)
                        {
                            final_map[row, col] = grass_A;
                        }
                        else
                        {
                            final_map[row, col] = grass_B;
                        }
                    }
                    if (snow_layer[row, col] == 1)
                    {
                        int choice = getRandomInt(0, 1);
                        if (choice == 0)
                        {
                            final_map[row, col] = snow_A;
                        }
                        else
                        {
                            final_map[row, col] = snow_B;
                        }
                    }
                    if (ice_layer[row, col] == 1)
                    {
                        int choice = getRandomInt(0, 1);
                        if (choice == 0)
                        {
                            final_map[row, col] = ice_A;
                        }
                        else
                        {
                            final_map[row, col] = ice_B;
                        }
                    }
                    if (blocks_layer[row, col] == 1)
                    {
                        final_map[row, col] = concrete_tile_A;
                    }
                    if (roads_layer[row, col] == 1)
                    {
                        final_map[row, col] = concrete_tile_A;
                    }
                    if (outer_bounds[row, col] == 1)
                    {
                        final_map[row, col] = outer_wall_A;
                    }
                }
            }

            // fill in any gaps with grass

            for (int row = 0; row < grid_size; row++)
            {
                for (int col = 0; col < grid_size; col++)
                {
                    if (final_map[row, col] == 'E')
                    {
                        final_map[row, col] = grass_A;
                    }
                }
            }

        }

        public char[,] getFinalMap()
        {
            return final_map;
        }

        public static int getRandomInt(int min, int max)
        {
            lock (getRandom)
            {
                return getRandom.Next(min, max);
            }
        }

        private void generate_roads()
        {
            int choice = getRandomInt(0, 8);

            if (choice <= 2) // intersection
            {
                int road_width = getRandomInt(2, 8);

                if (road_width % 2 != 0 )
                {
                    road_width++; // make even
                }

                for (int row = 0; row < grid_size; row++)
                {
                    for (int col = 0; col < grid_size; col++)
                    {
                        if (
                            (col >= (grid_size/2 - road_width/2)
                            && col <= (grid_size/2 + road_width/2))
                            ||
                            row >= (grid_size / 2 - road_width / 2)
                            && row <= (grid_size / 2 + road_width / 2)
                            )
                        {
                            roads_layer[row,col] = 1;
                        }
                        else
                        {
                            roads_layer[row, col] = 0;
                        }
                    }
                }

            }
            else if (choice >= 3 && choice <= 5) // diagonal
            {
                int road_width = getRandomInt(3, 6);

                int count = 0;

                // set to null

                for (int row = 0; row < grid_size; row++)
                {
                    for (int col = 0; col < grid_size; col++)
                    {
                        roads_layer[row, col] = 0;
                    }
                }

                // fill with diagonal

                for (int row = 0; row < grid_size; row++)
                {
                    for (int col = 0; col < grid_size; col++)
                    {
                        if (col >= count && col <= count + road_width)
                        {
                            if (col + road_width < grid_size)
                            {
                                for (int i = col; i < col + road_width; i++)
                                {
                                    roads_layer[row, i] = 1;
                                }
                            }
                        }
                        count++;
                    }
                }
            }
            else if (choice >= 6) // straight road
            {
                int road_width = getRandomInt(3, 6);

                int road_start = getRandomInt(0+road_width, (grid_size-road_width)-1);

                for (int row = 0; row < grid_size; row++)
                {
                    for (int col = 0; col < grid_size; col++)
                    {
                        if (col >= road_start && col < (road_start + road_width))
                        {
                            roads_layer[row, col] = 1;
                        }
                        else
                        {
                            roads_layer[row, col] = 0;
                        }
                    }
                }
            }

        }

        private void generate_blocks()
        {
            int block_width = getRandomInt(3, 8);
            int block_height = getRandomInt(3, 8);

            // set null
            for (int row = 0; row < grid_size; row++)
            {
                for (int col = 0; col < grid_size; col++)
                {
                    blocks_layer[row, col] = 0;
                }
            }

            //fill with blocks
            for (int row = 0; row < grid_size-block_width; row++)
            {
                for (int col = 0; col < grid_size-block_height; col++)
                {
                    // 1/14th chance of generating a block
                    int choice = getRandomInt(1, 20);

                    if (choice == 1)
                    {
                        // create a new block
                        for (int i = row; i < row + block_width; i++)
                        {
                            for (int j = col; i < col + block_height; i++)
                            {
                                blocks_layer[i, j] = 1;
                            }
                        }
                        
                    }
                }
            }
        }

        private void generate_ice()
        {
            int block_width = getRandomInt(3, 8);
            int block_height = getRandomInt(3, 8);

            // set null
            for (int row = 0; row < grid_size; row++)
            {
                for (int col = 0; col < grid_size; col++)
                {
                    ice_layer[row, col] = 0;
                }
            }

            //fill with blocks
            for (int row = 0; row < grid_size - block_width; row++)
            {
                for (int col = 0; col < grid_size - block_height; col++)
                {
                    // 1/18th chance of generating a block
                    int choice = getRandomInt(1, 18);

                    if (choice == 1)
                    {
                        // create a new block
                        for (int i = row; i < row + block_width; i++)
                        {
                            for (int j = col; i < col + block_height; i++)
                            {
                                int choice2 = getRandomInt(1,3);

                                if (choice2 == 1)
                                {
                                    ice_layer[i, j] = 1;
                                }
                            }
                        }

                    }
                }
            }
        }

        private void generate_snow()
        {
            int block_width = getRandomInt(3, 8);
            int block_height = getRandomInt(3, 8);

            // set null
            for (int row = 0; row < grid_size; row++)
            {
                for (int col = 0; col < grid_size; col++)
                {
                    snow_layer[row, col] = 0;
                }
            }

            //fill with blocks
            for (int row = 0; row < grid_size - block_width; row++)
            {
                for (int col = 0; col < grid_size - block_height; col++)
                {
                    // 1/7th chance of generating a block
                    int choice = getRandomInt(1, 7);

                    if (choice == 1)
                    {
                        // create a new block
                        for (int i = row; i < row + block_width; i++)
                        {
                            for (int j = col; i < col + block_height; i++)
                            {
                                int choice2 = getRandomInt(1, 2);

                                if (choice2 == 1)
                                {
                                    snow_layer[i, j] = 1;
                                }
                            }
                        }

                    }
                }
            }
        }

        private void generate_grass()
        {
            for (int row = 0; row < grid_size; row++)
            {
                for (int col = 0; col < grid_size; col++)
                {
                    int choice = getRandomInt(0, 1);
                    if (choice == 1)
                    {
                        grass_layer[row, col] = 1;
                    }
                }
            }
        }

        public void generate_dirt()
        {
            for (int row = 0; row < grid_size; row++)
            {
                for (int col = 0; col < grid_size; col++)
                {
                    int choice = getRandomInt(0, 3);
                    if (choice == 1)
                    {
                        dirt_layer[row, col] = 1;
                    }
                }
            }
        }

        public void generate_outer_bounds()
        {
            for (int row = 0; row < grid_size; row++)
            {
                for (int col = 0; col < grid_size; col++)
                {
                    if (row == 0 || col == 0 || row == grid_size-1 || col == grid_size-1)
                    {
                        outer_bounds[row, col] = 1;
                    }
                }
            }
        }
    }
}
